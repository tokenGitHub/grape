package com.example.grape;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class GrapeApplication {
    public static void main(String[] args) {
        SpringApplication.run(GrapeApplication.class, args);
    }
}
