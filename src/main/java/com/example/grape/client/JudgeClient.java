package com.example.grape.client;

import com.example.grape.dto.AnswerResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "judge")
public interface JudgeClient {

    @GetMapping(value = "judgement")
    AnswerResult judge(@RequestParam("questionId") int questionId, @RequestParam("context") String context);
}
