package com.example.grape.dto;

import lombok.Data;

@Data
public class AnswerResult {
    public enum ResultEnum {
        BUILD_ERROR, RESULT_ERROR, PART_ERROR, ACCEPT
    }

    private String resultMessage;
    private ResultEnum resultEnum;
    private int score;
}
