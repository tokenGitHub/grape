package com.example.grape.domain;

import com.example.grape.dto.AnswerResult;
import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import java.sql.Date;

@Data
@EntityScan
public class AnswerRecord {
    private int id;
    private int studentId;
    private int questionId;
    private int testId;
    private Date answerTime;
    private String answerContext;
    private AnswerResult result;
    private String IP;
}
