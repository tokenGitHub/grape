package com.example.grape.domain;

import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import java.sql.Date;


@Data
@EntityScan
public class Test {
    private int id;
    private int testPaperId;
    private Date createTime;
    private int duration;
    private Date startTime;
    private Date endTime;
}
