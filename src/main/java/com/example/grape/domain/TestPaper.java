package com.example.grape.domain;


import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@Data
@EntityScan
public class TestPaper {
    private int id;
    private int teacherId;
    private String testName;
    private String questionIdList;
    private int totalScore;
}
