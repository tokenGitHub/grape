package com.example.grape.domain;

import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@Data
@EntityScan
public class TestData {
    private int id;
    private int testId;
    private String input;
    private String output;
}
