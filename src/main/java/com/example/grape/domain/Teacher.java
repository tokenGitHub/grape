package com.example.grape.domain;

import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import java.sql.Date;


@Data
@EntityScan
public class Teacher {
    private int id;
    private String teacherName;
    private String password;
    private String email;
    private Date createTime;
}
