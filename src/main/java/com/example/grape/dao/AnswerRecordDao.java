package com.example.grape.dao;

import com.example.grape.domain.AnswerRecord;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AnswerRecordDao {
    void saveAnswerRecord(AnswerRecord answerRecord);
}
