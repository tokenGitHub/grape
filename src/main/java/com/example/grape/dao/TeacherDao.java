package com.example.grape.dao;

import com.example.grape.domain.Teacher;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface TeacherDao {
    void insertTeacher(@Param("teacher") Teacher teacher);
    String teacherLogin(@Param("teacherName") String teacherName);
}
