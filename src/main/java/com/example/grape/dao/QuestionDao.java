package com.example.grape.dao;

import com.example.grape.domain.Question;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface QuestionDao {
    List<Question> listQuestions();
    void insertQuestion(@Param("question") Question question);
    Question selectQuestionById(@Param("questionId") int questionId);
}
