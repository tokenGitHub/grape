package com.example.grape.dao;

import com.example.grape.domain.Student;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface StudentDao {
    void insertStudent(@Param("student") Student student);
    String studentLogin(@Param("studentName") String userName);
    Student selectStudentById(@Param("studentId") int studentId);
}
