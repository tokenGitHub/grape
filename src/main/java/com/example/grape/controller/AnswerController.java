package com.example.grape.controller;

import com.example.grape.domain.AnswerRecord;
import com.example.grape.domain.Question;
import com.example.grape.domain.Student;
import com.example.grape.client.JudgeClient;
import com.example.grape.service.AnswerRecordService;
import com.example.grape.service.QuestionService;
import com.example.grape.service.StudentService;
import com.example.grape.utills.DateUtil;
import com.example.grape.utills.IPUtil;
import com.example.grape.utills.JsonUtils;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Log
@RestController
@RequestMapping("answer")
public class AnswerController {

    @Autowired
    private QuestionService questionService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private JudgeClient judgeClient;
    @Autowired
    private AnswerRecordService answerRecordService;

    @GetMapping("postAnswer")
    public Object postAnswer(@RequestParam int studentId, @RequestParam int questionId, @RequestParam String answerContext, HttpServletRequest request){
        Student student = studentService.selectStudentById(studentId);
        Question question = questionService.selectQuestionById(questionId);
        AnswerRecord answerRecord = new AnswerRecord();
        answerRecord.setAnswerContext(answerContext);
        answerRecord.setQuestionId(question.getId());
        answerRecord.setStudentId(student.getId());
        answerRecord.setAnswerTime(DateUtil.getTodayDate());
        answerRecord.setResult(judgeClient.judge(questionId,answerContext));
        answerRecord.setIP(IPUtil.getIpAddr(request));
        answerRecordService.saveAnswerRecord(answerRecord);

        return JsonUtils.getSuccessResult(answerRecord);
    }
}
