package com.example.grape.controller;

import com.example.grape.domain.Student;
import com.example.grape.service.RedisService;
import com.example.grape.service.StudentService;
import com.example.grape.utills.JsonUtils;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;

@RestController
@Log
@RequestMapping("student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private RedisService redisService;

    @GetMapping("register")
    public Object userRegister(@RequestParam String studentName, @RequestParam String password, @RequestParam String email){
        Date date = new Date(new java.util.Date().getTime());
        Student student = parseStudentByData(studentName, password, email, date);
        studentService.insertStudent(student);
        return JsonUtils.getSuccessResult(student);
    }

    @GetMapping("login")
    public Object login(@RequestParam String studentName, @RequestParam String password){
        String userCode = studentService.studentLogin(studentName, password);
        if(userCode != null){
            redisService.setStudentLoginCode(studentName, userCode);
            return JsonUtils.getSuccessResult(userCode);
        }else{
            return JsonUtils.getFailResult(new Exception("Exception : 账号或密码错误"));
        }
    }

    private Student parseStudentByData(String studentName, String password, String email, Date createTime){
        Student student = new Student();
        student.setStudentName(studentName);
        student.setEmail(email);
        student.setPassword(password);
        student.setCreateTime(createTime);
        return student;
    }
}
