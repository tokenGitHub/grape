package com.example.grape.controller;

import com.example.grape.domain.Teacher;
import com.example.grape.service.RedisService;
import com.example.grape.service.TeacherService;
import com.example.grape.utills.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;

@RestController
@RequestMapping("teacher")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    @Autowired
    private RedisService redisService;

    @PostMapping("register")
    public Object userRegister(@RequestParam String teacherName, @RequestParam String password, @RequestParam String email){
        Date date = new Date(new java.util.Date().getTime());
        Teacher teacher = parseTeacherByData(teacherName, password, email, date);
        teacherService.insertTeacher(teacher);
        return JsonUtils.getSuccessResult(teacher);
    }

    @GetMapping("login")
    public Object login(@RequestParam String teacherName, @RequestParam String password){
        String teacherCode = teacherService.teacherLogin(teacherName, password);
        if(teacherCode != null){
            redisService.setStudentLoginCode(teacherName, teacherCode);
            return JsonUtils.getSuccessResult(teacherCode);
        }else{
            return JsonUtils.getFailResult(new Exception("Exception : 账号或密码错误"));
        }
    }

    private Teacher parseTeacherByData(String teacherName, String password, String email, Date createTime){
        Teacher teacher = new Teacher();
        teacher.setTeacherName(teacherName);
        teacher.setEmail(email);
        teacher.setPassword(password);
        teacher.setCreateTime(createTime);
        return teacher;
    }
}
