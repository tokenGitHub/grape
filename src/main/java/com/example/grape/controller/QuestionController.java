package com.example.grape.controller;

import com.example.grape.domain.Question;
import com.example.grape.service.QuestionService;
import com.example.grape.utills.DateUtil;
import com.example.grape.utills.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;

@RestController
@RequestMapping("question")
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @GetMapping("listQuestions")
    public Object listQuestions(){
        return JsonUtils.getSuccessResult(questionService.listQuestions());
    }

    @GetMapping("insertQuestion")
    public Object insertQuestion(@RequestParam Integer teacherId,
                                 @RequestParam String title,
                                 @RequestParam String inputExample,
                                 @RequestParam String outputExample,
                                 @RequestParam String dataRange,
                                 @RequestParam Integer totalScore,
                                 @RequestParam String context){
        try{
            Question question = new Question();
            question.setTeacherId(teacherId);
            question.setInputExample(inputExample);
            question.setOutputExample(outputExample);
            question.setTitle(title);
            question.setDataRange(dataRange);
            question.setCreateDate(DateUtil.getTodayDate());
            question.setTotalScore(totalScore);
            question.setContext(context);
            questionService.insertQuestion(question);
        }catch (Exception e){
            return JsonUtils.getFailResult(e);
        }
        return JsonUtils.getSuccessResult("试题提交成功，等待审核");
    }

}
