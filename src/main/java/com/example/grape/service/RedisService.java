package com.example.grape.service;

public interface RedisService {
    void saveEmailVerificationCode(String mail, String verification);
    String getEmailVerificationCode(String mail);
    void setStudentLoginCode(String userName, String loginCode);
    String getStudentLoginCode(String userName);
    String getTeacherLoginCode(String teacherName);
}
