package com.example.grape.service;

import com.example.grape.utills.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RedisServiceImpl implements RedisService{
    @Autowired
    private RedisUtil<String> redisUtil;

    private final String STUDENT_LOGIN_CODE = "STUDENT_LOGIN_CODE_";
    private final String TEACHER_LOGIN_CODE = "TEACHER_LOGIN_CODE_";
    private final String VERIFICATION_CODE = "VERIFICATION_CODE_";

    @Override
    public void saveEmailVerificationCode(String mail, String verification){
        redisUtil.set(VERIFICATION_CODE + mail, verification, 60L);
    }

    @Override
    public String getEmailVerificationCode(String mail){
        String value = null;
        try {
            value = (String)redisUtil.get(VERIFICATION_CODE + mail);
        }catch (Exception e){
            e.printStackTrace();
        }
        return value;
    }

    @Override
    public void setStudentLoginCode(String studentName, String loginCode){
        try {
            redisUtil.set(STUDENT_LOGIN_CODE + studentName, loginCode, 60 * 30);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public String getStudentLoginCode(String studentName){
        return (String) redisUtil.get(STUDENT_LOGIN_CODE + studentName);
    }

    @Override
    public String getTeacherLoginCode(String teacherName){
        return (String) redisUtil.get(TEACHER_LOGIN_CODE + teacherName);
    }
}
