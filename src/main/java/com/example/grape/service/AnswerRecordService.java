package com.example.grape.service;

import com.example.grape.domain.AnswerRecord;

public interface AnswerRecordService {
    void saveAnswerRecord(AnswerRecord answerRecord);
}
