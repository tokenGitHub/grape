package com.example.grape.service;


import com.example.grape.dao.StudentDao;
import com.example.grape.domain.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.sql.Date;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDao studentDao;

    @Override
    @Transactional
    public void insertStudent(Student student){
        studentDao.insertStudent(student);
    }

    @Override
    @Transactional
    public String studentLogin(String studentName, String password){
        Date date = new Date(new java.util.Date().getTime());
        String select = studentDao.studentLogin(studentName);
        if(select != null && select.equals(password)) {
            String md5 = DigestUtils.md5DigestAsHex((studentName + password + date.toString()).getBytes());
            return md5;
        }else{
            return null;
        }
    }

    @Override
    @Transactional
    public Student selectStudentById(int studentId){
        return studentDao.selectStudentById(studentId);
    }
}
