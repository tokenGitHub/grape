package com.example.grape.service;

import com.example.grape.dao.QuestionDao;
import com.example.grape.domain.Question;
import com.example.grape.dto.AnswerResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionDao questionDao;

    @Override
    @Transactional
    public List<Question> listQuestions(){
        return questionDao.listQuestions();
    }

    @Override
    @Transactional
    public void insertQuestion(Question question){
        questionDao.insertQuestion(question);
    }

    @Override
    @Transactional
    public Question selectQuestionById(int questionId){
        return questionDao.selectQuestionById(questionId);
    }
}
