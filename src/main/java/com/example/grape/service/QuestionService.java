package com.example.grape.service;

import com.example.grape.domain.Question;
import com.example.grape.dto.AnswerResult;

import java.util.List;

public interface QuestionService {
    List<Question> listQuestions();
    void insertQuestion(Question question);
    Question selectQuestionById(int questionId);
}
