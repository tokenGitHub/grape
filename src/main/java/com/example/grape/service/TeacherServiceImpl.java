package com.example.grape.service;

import com.example.grape.dao.TeacherDao;
import com.example.grape.domain.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.sql.Date;

@Service
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private TeacherDao teacherDao;

    @Override
    @Transactional
    public void insertTeacher(Teacher teacher){
        teacherDao.insertTeacher(teacher);
    }

    @Override
    @Transactional
    public String teacherLogin(String teacherName, String password){
        Date date = new Date(new java.util.Date().getTime());
        String select = teacherDao.teacherLogin(teacherName);
        if(select != null && select.equals(password)) {
            String md5 = DigestUtils.md5DigestAsHex((teacherName + password + date.toString()).getBytes());
            return md5;
        }else{
            return null;
        }
    }
}
