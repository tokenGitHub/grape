package com.example.grape.service;

import com.example.grape.dao.AnswerRecordDao;
import com.example.grape.domain.AnswerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AnswerRecordServiceImpl implements AnswerRecordService {

    @Autowired
    AnswerRecordDao answerRecordDao;

    @Override
    @Transactional
    public void saveAnswerRecord(AnswerRecord answerRecord){
        answerRecordDao.saveAnswerRecord(answerRecord);
    }
}
