package com.example.grape.service;

import com.example.grape.domain.Teacher;

public interface TeacherService {
    void insertTeacher(Teacher teacher);
    String teacherLogin(String teacherName, String password);
}
