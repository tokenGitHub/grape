package com.example.grape.service;

import com.example.grape.domain.Student;

public interface StudentService {
    void insertStudent(Student student);
    String studentLogin(String studentName, String password);
    Student selectStudentById(int studentId);
}
