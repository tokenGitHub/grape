package com.example.judge.service;

import com.example.judge.dto.AnswerResult;

public interface JudgeService {
    AnswerResult judge(int studentId, int questionId, String context) throws InterruptedException;
}
