package com.example.judge.service;

import com.example.judge.Utils.FileUtil;
import com.example.judge.dao.QuestionDao;
import com.example.judge.dao.StudentDao;
import com.example.judge.domain.Question;
import com.example.judge.domain.TestData;
import com.example.judge.dto.AnswerResult;
import com.example.judge.thread.RunFileThread;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Log
@Service
public class JudgeServiceImpl implements JudgeService {

    @Autowired
    StudentDao studentDao;

    @Autowired
    QuestionDao questionDao;

    @Override
    @Transactional
    public AnswerResult judge(int studentId, int questionId, String context) throws InterruptedException {
        Question question = questionDao.selectQuestionById(questionId);
        List<TestData> testDataList = questionDao.listTestDataByQuestionId(questionId);

        AnswerResult answerResult1 = null;
        String fileName = "Main" ;
        double totalScore = 0;

        if(FileUtil.saveFile(fileName + ".java" ,context)) {
            String buildMessage = FileUtil.buildFile(fileName + ".java");

            if(buildMessage == null || buildMessage.equals("")) {
                RunFileThread runFileThread = new RunFileThread(testDataList, context, question);
                runFileThread.start();

                //等待判题程序运行完成
                while(runFileThread.isAlive()) {
                    this.wait();
                }

                answerResult1 = runFileThread.getAnswerResult();
            }else{
                answerResult1 = new AnswerResult();
                answerResult1.setResultMessage(buildMessage);
                answerResult1.setResultEnum(AnswerResult.ResultEnum.BUILD_ERROR);
                answerResult1.setScore(0);
                log.info(buildMessage);
            }
            return answerResult1;
        }

        return null;
    }
}
