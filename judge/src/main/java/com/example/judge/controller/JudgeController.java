package com.example.judge.controller;

import com.example.judge.dto.AnswerResult;
import com.example.judge.service.JudgeService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@Log
@RestController
public class JudgeController {

    @Autowired
    JudgeService judgeService;

    @GetMapping(value = "judgement")
    public AnswerResult judge(@RequestParam("studentId") int studentId, @RequestParam("questionId") int questionId){
//    public AnswerResult judge(@RequestParam("studentId") int studentId, @RequestParam("questionId") int questionId , @RequestParam("context") String context){
        String context = "\nimport java.util.Scanner;\n" +
                "public class Main{\n" +
                "         public static void main(String args[]) {\n" +
                "            Scanner input = new Scanner(System.in);\n" +
                "            int n = input.nextInt();\n" +
                "            System.out.println(n);\n" +
                "        }\n" +
                "    }";

        log.info(context);
        AnswerResult answerResult = judgeService.judge(studentId, questionId, context);
        return answerResult;
    }

}
