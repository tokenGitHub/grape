package com.example.judge.dao;

import com.example.judge.domain.Question;
import com.example.judge.domain.TestData;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface QuestionDao {
    Question selectQuestionById(@Param("questionId") int questionId);
    List<TestData> listTestDataByQuestionId(@Param("questionId") int questionId);
}
