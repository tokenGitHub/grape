package com.example.judge.dao;


import com.example.judge.domain.Student;
import feign.Param;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StudentDao {
    Student selectStudentById(@Param("studentId") int studentId);
}
