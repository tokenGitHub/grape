package com.example.judge.thread;

import com.example.judge.Utils.FileUtil;
import com.example.judge.domain.TestData;
import com.example.judge.dto.AnswerResult;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;

@Log
@AllArgsConstructor
public class JudgeThread extends Thread {
    private AnswerResult.ResultEnum resultEnum = null;
    private String fileName = null;
    private TestData data = null;

    public JudgeThread(String fileName, TestData data){
        this.fileName = fileName;
        this.data = data;
    }

    public AnswerResult.ResultEnum getResult(){
        return resultEnum;
    }

    @Override
    public void run() {
        super.run();
        String answerResultStr = FileUtil.runFile(fileName, data.getInput());
        if (answerResultStr != null && answerResultStr.trim().equals(data.getOutput().trim())) {
            resultEnum = AnswerResult.ResultEnum.ACCEPT;
        }
        log.info(answerResultStr);
    }
}
