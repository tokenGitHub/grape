package com.example.judge.thread;

import com.example.judge.domain.Question;
import com.example.judge.domain.TestData;
import com.example.judge.dto.AnswerResult;
import lombok.extern.java.Log;

import java.util.ArrayList;
import java.util.List;

@Log
public class RunFileThread extends Thread{
    private List<TestData> list;
    private String context;
    private Question question;
    private MonitorThread monitorThread;
    private AnswerResult answerResult;

    public RunFileThread(List<TestData> list, String context, Question question){
        this.list = list;
        this.context = context;
        this.question = question;
    }

    @Override
    public void run() {
        super.run();
        List<JudgeThread> threadList = new ArrayList<>();

        for(TestData testData : list){
            threadList.add(new JudgeThread(context, testData));
        }
        monitorThread = new MonitorThread(threadList, question);
        try {
            monitorThread.start();
            while(monitorThread.isAlive()) {
                this.wait();
            }
            answerResult = monitorThread.getAnswerResult();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public AnswerResult getAnswerResult(){
        return answerResult;
    }
}
