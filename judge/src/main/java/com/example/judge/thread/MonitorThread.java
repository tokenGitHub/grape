package com.example.judge.thread;

import com.example.judge.domain.Question;
import com.example.judge.dto.AnswerResult;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;

import java.util.List;

@Log
@AllArgsConstructor
public class MonitorThread extends Thread {
    private List<JudgeThread> judgeThreadList;
    private Question question;

    @Override
    public void run() {
        super.run();

        for(JudgeThread judgeThread : judgeThreadList){
            judgeThread.start();
        }

        try {
            Thread.sleep(question.getMaxTime());
            //TODO 处理超时程序、超内存程序
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    //TODO处理结果
    public AnswerResult getAnswerResult(){
        return null;
    }
}
