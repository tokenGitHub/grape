package com.example.judge.Utils;

import lombok.extern.java.Log;

import java.io.*;

@Log
public class FileUtil {

    public static boolean saveFile(String fileName, String context){
        File file = new File(fileName);

        log.info("-------------------- save file start ----------------------");
        try(FileWriter writer = new FileWriter(file)) {
            writer.write(context);
        }catch (IOException e){
            log.info(e.getMessage());
            e.printStackTrace();
            return false;
        }

        log.info("------------------- save file end --------------------------");
        return true;
    }

    public static String buildFile(String fileName){
        String result = "";

        log.info("============ build start ==================");
        try {
            result = execCmd(" javac " + fileName, null, null);
        }catch (Exception e){
            e.printStackTrace();
        }

        log.info("build result : " + result);
        log.info("=================== build end ====================");
        return result;
    }

    public static String runFile(String fileName, String args){
        String result = null;

        log.info(" ========================== run start =========================");
        try {
            result = execCmd(" java " + fileName, null, args);
        }catch (Exception e){
            e.printStackTrace();
        }
        log.info(" run result : " + result);
        log.info("========================== run end ============================");
        return result;
    }

    private static String execCmd(String cmd, File dir, String args) throws Exception {
        StringBuilder result = new StringBuilder();

        Process process = null;
        BufferedReader bufferedReader = null;
        BufferedReader bufferErrorReader = null;
        BufferedWriter bufferedWriter = null;

        try {
            process = Runtime.getRuntime().exec(cmd, null, dir);
            if(args != null){
                bufferedWriter = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
                bufferedWriter.write(args);
                bufferedWriter.close();
                log.info(args);
            }
            process.waitFor();

            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream(), "GBK"));
            bufferErrorReader = new BufferedReader(new InputStreamReader(process.getErrorStream(), "GBK"));

            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                result.append(line);
            }
            while ((line = bufferErrorReader.readLine()) != null) {
                result.append(line);
            }
        } finally {
            closeStream(bufferedReader);
            closeStream(bufferErrorReader);
            closeStream(bufferedWriter);
            if (process != null) {
                process.destroy();
            }
        }

        return result.toString();
    }

    private static void closeStream(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
