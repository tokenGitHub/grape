package com.example.judge.domain;

import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import java.sql.Date;


@Data
@EntityScan
public class Student {
    private int id;
    private String password;
    private String studentName;
    private String email;
    private Date createTime;
}
