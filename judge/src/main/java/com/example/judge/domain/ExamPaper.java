package com.example.judge.domain;


import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@Data
@EntityScan
public class ExamPaper {
    private int id;
    private int teacherId;
    private String testName;
    private String questionIdList;
    private int totalScore;
}
