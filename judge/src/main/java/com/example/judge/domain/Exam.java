package com.example.judge.domain;

import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import java.sql.Date;


@Data
@EntityScan
public class Exam {
    private int id;
    private int testPaperId;
    private Date createTime;
    private int duration;
    private Date startTime;
    private Date endTime;

}
