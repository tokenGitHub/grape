package com.example.judge.domain;


import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import java.sql.Date;

@Data
@EntityScan
public class Question {
    private int id;
    private int teacherId;
    private String title;
    private String context;
    private String inputExample;
    private String outputExample;
    private String dataRange;
    private Date createDate;
    private int totalScore;
    private int maxMemory;
    private long maxTime;
}
