package com.example.judge.domain;

import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@Data
@EntityScan
public class TestData {
    private int id;
    private int questionId;
    private String input;
    private String output;
}
